#include "esp_log.h"
#include "esp_err.h"
#include "eth.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_netif.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "freertos/event_groups.h"
#include "wiphy.h"
#include "veryhotspot.h"
#include "esp_event.h"
#include "driver/gpio.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "lwip/lwip_napt.h"

#include "pppos_client.h"
const char *TAG = "MAIN";
esp_netif_dhcp_status_t status;
void app_main()
{
    // gpio_reset_pin(GPIO_NUM_34);
    // gpio_set_direction(GPIO_NUM_34, GPIO_MODE_OUTPUT);
    // gpio_set_level(GPIO_NUM_34, 1);
    // Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    // Initialize TCP/IP network interface (should be called only once in application)
    ESP_ERROR_CHECK(esp_netif_init());
    // Create default event loop that running in background
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    // real

    ESP_LOGI(TAG, "stated");
    start_eth();

    ESP_LOGI(TAG, "ended eth");
    ESP_LOGI(TAG, "started wfi");
    // start_wifi();
    ip_napt_enable(wifi_ip.ip.addr, 1);
    ESP_LOGI(TAG, "ended wifi");

    while (1)
    {
        esp_netif_dhcps_get_status(eth_netif, &status);
        ESP_LOGI(TAG, "server status %d", status);
        esp_netif_dhcpc_get_status(eth_netif, &status);
        ESP_LOGI(TAG, "status client %d", status);

        vTaskDelay(pdMS_TO_TICKS(1000)); // spec: 1s delay for the modem to recognize the escape sequence
    }

    // fake
    // ESP_LOGI(TAG, "sta pppos");

    // xTaskCreate(start_pppos, "pppos", 4096, NULL, 2, NULL);
    // ESP_LOGI(TAG, "sta hot");

    // startveryhotspot();

    // ip_napt_enable(esp_ip4addr_aton("192.168.4.1"), true);
}